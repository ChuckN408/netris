module git.sr.ht/~tslocum/netris

go 1.13

require (
	git.sr.ht/~tslocum/cview v0.2.2-0.20200103004846-1207c71f9f20
	github.com/anmitsu/go-shlex v0.0.0-20161002113705-648efa622239 // indirect
	github.com/creack/pty v1.1.9
	github.com/flynn/go-shlex v0.0.0-20150515145356-3f9db97f8568 // indirect
	github.com/gdamore/tcell v1.3.0
	github.com/gliderlabs/ssh v0.2.2
	github.com/lucasb-eyer/go-colorful v1.0.3 // indirect
	github.com/mattn/go-isatty v0.0.11
	github.com/mattn/go-runewidth v0.0.7 // indirect
	golang.org/x/crypto v0.0.0-20191227163750-53104e6ec876
	golang.org/x/sys v0.0.0-20200103143344-a1369afcdac7 // indirect
)
